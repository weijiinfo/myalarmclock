//
//  NotifitionModel.h
//  MyAlarmClock
//
//  Created by jiwei on 15/5/22.
//  Copyright (c) 2015年 weiji.info. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifitionModel : NSObject

@property (nonatomic, strong)NSDate *fireDate;//触发的时间
@property (nonatomic)NSInteger selectedWeek;//选择的周

@end
