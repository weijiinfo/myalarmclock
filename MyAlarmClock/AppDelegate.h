//
//  AppDelegate.h
//  MyAlarmClock
//
//  Created by jiwei on 15/5/18.
//  Copyright (c) 2015年 weiji.info. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

