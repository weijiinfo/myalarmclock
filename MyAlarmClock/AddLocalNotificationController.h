//
//  AddLocalNotificationController.h
//  MyAlarmClock
//
//  Created by jiwei on 15/5/18.
//  Copyright (c) 2015年 weiji.info. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddLocalNotificationController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *timeTextField;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
