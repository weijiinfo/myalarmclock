//
//  main.m
//  MyAlarmClock
//
//  Created by jiwei on 15/5/18.
//  Copyright (c) 2015年 weiji.info. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
