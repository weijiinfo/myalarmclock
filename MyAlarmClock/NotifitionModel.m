//
//  NotifitionModel.m
//  MyAlarmClock
//
//  Created by jiwei on 15/5/22.
//  Copyright (c) 2015年 weiji.info. All rights reserved.
//

#import "NotifitionModel.h"

@implementation NotifitionModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        //初始化的默认值
        self.fireDate = [NSDate date];
        self.selectedWeek = 0;
    }
    return self;
}

@end
